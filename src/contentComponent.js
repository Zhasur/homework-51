import React, {Component} from 'react'
import Movie from './moviePosters';
import MenuLists from './menuLists';

class Content extends Component {
    render() {
        return (
            <div className="main-block">
                <div className="menu-bar">
                    <form>
                        <input type="text" className="search-area" placeholder="Enter movie name"/>
                        <button className="search-btn">Search</button>
                    </form>
                    <ul className="menu-main-list">
                        <MenuLists text="Биографии"/>
                        <MenuLists text="Боевики"/>
                        <MenuLists text="Вестерны"/>
                        <MenuLists text="Военные"/>
                        <MenuLists text="Детективы"/>
                        <MenuLists text="Детские"/>
                        <MenuLists text="Исторические"/>
                        <MenuLists text="Комедии"/>
                        <MenuLists text="Криминалы"/>
                        <MenuLists text="Мультфильмы"/>
                        <MenuLists text="Мюзиклы"/>
                        <MenuLists text="Семейные"/>
                        <MenuLists text="Спортивные"/>
                        <MenuLists text="Спортивные"/>
                        <MenuLists text="Ужасы"/>
                        <MenuLists text="Фантастика"/>
                    </ul>
                </div>
                <div className="content container">
                    <h1 className="main-title">My favorite movies</h1>
                    <Movie name="Веном" date="2018" src="http://kinogo.cc/uploads/posts/2018-10/1539179130_venom.jpg"/>
                    <Movie name="Тёмный Рыцарь" date="2008"
                           src="https://m.media-amazon.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_SY1000_CR0,0,675,1000_AL_.jpg"/>
                    <Movie name="Фантастические твари" date="2018"
                           src="https://m.media-amazon.com/images/M/MV5BZjFiMGUzMTAtNDAwMC00ZjRhLTk0OTUtMmJiMzM5ZmVjODQxXkEyXkFqcGdeQXVyMDM2NDM2MQ@@._V1_.jpg" />
                    <Movie name="Невидимый гость" date="2017"
                           src="http://kinogo.cc/uploads/posts/2017-09/1506440120_nevidimyj-gost-2016.jpg" />
                    <Movie name="Крёстный отец" date="1972"
                           src="https://m.media-amazon.com/images/M/MV5BM2MyNjYxNmUtYTAwNi00MTYxLWJmNWYtYzZlODY3ZTk3OTFlXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SY1000_CR0,0,704,1000_AL_.jpg" />
                    <Movie name="Крёстный отец 2" date="1974"
                           src="https://m.media-amazon.com/images/M/MV5BMWMwMGQzZTItY2JlNC00OWZiLWIyMDctNDk2ZDQ2YjRjMWQ0XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SY1000_CR0,0,701,1000_AL_.jpg" />
                    <Movie name="Властелин колец: Возврашение короля" date="2003"
                           src="https://m.media-amazon.com/images/M/MV5BNzA5ZDNlZWMtM2NhNS00NDJjLTk4NDItYTRmY2EwMWZlMTY3XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SY1000_CR0,0,675,1000_AL_.jpg" />
                </div>
            </div>
        );
    }
}

export default Content;