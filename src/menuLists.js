import React from 'react';

const MenuLists = (props) => {
    return React.createElement('li', {className: 'menu-list'},
        React.createElement('a', {href: '#', className: 'menu-link'}, props.text));
};

export default MenuLists;