import React from 'react';


const Movie = (props) => {
    return React.createElement('div', {className: 'poster'},
        React.createElement('a', {className: 'movie-link', href: '#'},
            React.createElement ('img', {className: 'poster-img', src: props.src})),
        React.createElement ('div', {className: 'poster-info'},
            React.createElement('h4', {className: 'movie-name'}, props.name),
            React.createElement('p', {className: 'movie-date'}, '(' + props.date +')'),)
    );
};

export default Movie;