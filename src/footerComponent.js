import React, {Component} from 'react'

class Footer extends Component {
    render() {
        return (
            <footer className="footer">
                <span className="footer-span container">All contents © copyright 2018. All rights reserved Designed by: <i>Attractor students</i></span>
            </footer>
        );
    }
}

export default Footer;