import React, {Component} from 'react'
import HeaderNav from "./headerNavbars";

class Header extends Component {
    render() {
        return (
            <header className="header">
                <nav className="nav container">
                    <a href="#" className="icon">Movie <i>online</i></a>
                    <ul className="header-main-list">
                        <HeaderNav text="Home"/>
                        <HeaderNav text="Popular"/>
                        <HeaderNav text="New"/>
                        <HeaderNav text="Favorite"/>
                    </ul>
                </nav>
            </header>
        );
    }
}

export default Header;