import React, { Component, Fragment } from 'react';
import './App.css';
import Header from "./headerComponent";
import Content from "./contentComponent";
import Footer from "./footerComponent";

class App extends Component {
  render() {
    return (
       <Fragment>
           <Header/>
           <Content/>
           <Footer/>
       </Fragment>
    );
  }
}

export default App;
