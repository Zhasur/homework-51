import React from 'react';

const HeaderNav = (props) => {
    return React.createElement('li', {className: 'header-list'},
        React.createElement('a', {href: '#', className: 'header-nav', }, props.text));
};

export default HeaderNav;